// B1: Import mongooseJS
const mongoose = require("mongoose");

// B2: Khai báo Schema từ thư viện mongoose
const Schema = mongoose.Schema;

// B3: Khởi tạo 1 schema với các thuộc tính được yêu cầu
const courseSchema = new Schema({
    _id: {
        type: mongoose.Types.ObjectId
    },
    title: {
        type: String,
        required: true,
        unique: true
    },
    description: {
        type: String,
        required: false
    },
    noStudent: {
        type: Number,
        default: 0
    },
    createdAt: {
        type: Date,
        default: Date.now()
    },
    updatedAt: {
        type: Date,
        default: Date.now()
    },
    reviews: [ 
        {
            type: mongoose.Types.ObjectId,
            ref: "review"
        }
    ]
});

// B4: Export ra một model nhờ Schema vừa khai báo
module.exports = mongoose.model("course", courseSchema);